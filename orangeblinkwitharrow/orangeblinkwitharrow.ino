#include <Adafruit_NeoPixel.h>

// the data pin for the NeoPixels
int neoPixelPin = 6;

// How many NeoPixels we will be using, charge accordingly
int numPixels = 40;

// Instatiate the NeoPixel from the ibrary
Adafruit_NeoPixel strip = Adafruit_NeoPixel(numPixels, neoPixelPin, NEO_GRB + NEO_KHZ800);

// Color set #1
int r1 = 255;
int g1 = 153;
int b1 = 000;

// Color set #2
int r2 = 000;
int g2 = 000;
int b2 = 000;

// our button
int switchPin = 6;

void setup() {
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
     activate();
}


void activate() {
 strip.setBrightness(20);
       strip.setPixelColor(2, r1, g1, b1);
       strip.setPixelColor(9, r1, g1, b1);
       strip.setPixelColor(16, r1, g1, b1);
       strip.setPixelColor(17, r1, g1, b1);
       strip.setPixelColor(18, r1, g1, b1);
       strip.setPixelColor(19, r1, g1, b1);
       strip.setPixelColor(20, r1, g1, b1);
       strip.setPixelColor(21, r1, g1, b1);
       strip.setPixelColor(22, r1, g1, b1);
       strip.setPixelColor(25, r1, g1, b1);
       strip.setPixelColor(34, r1, g1, b1);
       
      
strip.show();
  delay(500);

       strip.setPixelColor(2, r2, g2, b2);
       strip.setPixelColor(9, r2, g2, b2);
       strip.setPixelColor(16, r2, g2, b2);
       strip.setPixelColor(17, r2, g2, b2);
       strip.setPixelColor(18, r2, g2, b2);
       strip.setPixelColor(19, r2, g2, b2);
       strip.setPixelColor(20, r2, g2, b2);
       strip.setPixelColor(21, r2, g2, b2);
       strip.setPixelColor(22, r2, g2, b2);
       strip.setPixelColor(25, r2, g2, b2);
       strip.setPixelColor(34, r2, g2, b2);
       
      
strip.show();
  delay(500);
}

