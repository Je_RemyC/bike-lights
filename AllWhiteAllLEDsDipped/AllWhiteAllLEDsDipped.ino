#include <Adafruit_NeoPixel.h>
 
// the data pin for the NeoPixels
int neoPixelPin = 6;

// How many NeoPixels we will be using, charge accordingly
int numPixels = 40;

// Instatiate the NeoPixel from the ibrary
Adafruit_NeoPixel strip = Adafruit_NeoPixel(numPixels, neoPixelPin, NEO_GRB + NEO_KHZ800);

// Color set #1
int r1 = 255;
int g1 = 255;
int b1 = 255;

// our button
int switchPin = 6;

void setup() {
  strip.begin();  // initialize the strip
  strip.show();   // make sure it is visible
  strip.clear();  // Initialize all pixels to 'off'
}

void loop() {
     activate();
}

// turn the lights on
void activate() {
   // Activate color set #1
    for( int i = 0; i < 40; i++ ) {
       strip.setPixelColor(i, r1, g1, b1);
       strip.setBrightness(03);
    }
    
  strip.show();
}
